@echo off
echo.
echo                《 Office 2019 自動安裝程式 》
echo ----------------------------------------------------------------
echo.
echo   安裝 Office 2019 ProPlus (64bit)
echo.
echo ----------------------------------------------------------------
set /p menu="是否要安裝 ? (Y/N):"
if %menu%==Y goto Yes
if %menu%==y goto Yes
if %menu%==N goto No
if %menu%==n goto No
exit

:Yes
echo   《正在安裝.....請稍後》
echo setup /configure configuration.xml
pushd \\stusoftware.tku.edu.tw\software\MS軟體\Office 2019 Professional Plus 64 位元 中文版
setup /configure configuration.xml
popd
echo   《 安裝成功 》
echo.
echo.
pause

exit